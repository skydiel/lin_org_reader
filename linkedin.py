import requests
from urllib.parse import urlencode
from datetime import date

linkedin_authz_base_url = 'https://www.linkedin.com/uas/oauth2/authorization'
linkedin_token_url = 'https://www.linkedin.com/uas/oauth2/accessToken'

class Linkedin(object):
	@classmethod
	def get_authorization_url(cls, client_id, redirect_uri):
		"""
		Build an authorizorization URL
		Params: client_id: your app's client id
				redirect_uri: where the authorization code will be sent
		Returns: a string with the authorization URL
		"""
		q = {
			'response_type': 'code',
			'client_id': client_id,
			'redirect_uri': redirect_uri,
			'state': 'fdgs55f6ds5g7d6f5g' 
		}
		return linkedin_authz_base_url + '?' + urlencode(q)


	@classmethod
	def get_token(cls, client_id, client_secret, redirect_uri, authorization_code):
		"""
		Retrieves the access token from the authorization server
		Params: client_id: your app's client id
				client_secret: your app's client secret
				redirect_uri: where the authorization code will be sent
				authorization_code: a code received by calling get_authorization_url() merthod
		Returns: a json containing the access token and other info or None on error
		"""
		h = { 
			'Content-Type': 'application/x-www-form-urlencoded'
		}
		b = {
			'grant_type': 'authorization_code',
			'code': authorization_code,
			'redirect_uri': redirect_uri,
			'client_id': client_id,
			'client_secret': client_secret
		}
		resp = requests.post(linkedin_token_url, headers=h, data=b)
		return resp.json['access_token'] if resp.status_code == 200 else None


	@classmethod
	def mock_company_data(cls, name):
		import faker
		import random

		fake = faker.Faker()
		ri = random.randint(1,9999)
		company_size = f'SIZE_{ri}_TO_{random.randint(ri,99999)}'

		return {
			'entityStatus': random.choice(['ACTIVE','INACTIVE','HACKED','FEDERAL INVESTIGATION']),
			'vanityName': name, 
			'id': random.randint(1,9999999), 
			'industries': [],
			'foundedOn': {
				'year': random.choice(range(1800,date.today().year))
			}, 
			'website': { 
		        "preferredLocale": {
        		    "country": "US",
            		"language": "en"
        		},
        		"localized": {
            		"en_US": fake.url()
        		}
    		}, 
			'specialties': [
		        {
		            "locale": {
		                "country": "US",
		                "language": "en"
		            },
		            "tags": [ fake.bs() ]
		        }
		    ],
			'staffCountRange': company_size
		}

	def __init__(self, token):
		self.token = token


	def searchCompanies(self, companyNames):
		"""
		Retrieves company data given a list of company names
		Params: companyNames: a list of company names to be searched
		Returns: a list of found companies and their data
		"""
		_attribs = ['entityStatus', 'vanityName', 'id', 'industries', 'foundedOn', 'website', 'specialties', 'staffCountRange']

		companyNames = searchCompanies if isinstance(companyNames, dict) else list(companyNames)
		companyData = []
		for company in companyNames:
			resp = requests.get('https://api.linkedin.com/v2/organizations?q=vanityName&vanityName='+company)
			data = filter_dict(resp.json, _attribs) \
					if resp.status_code == 200 \
					else self.__class__.mock_company_data(company)

			companyData.append(data)

		return companyData
