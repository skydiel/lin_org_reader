import click
import requests
import json
from linkedin import Linkedin
from utils import clean_split

@click.group()
def cli():
	pass

@cli.command()
@click.option('--names', default="", help='a list of comma-separated company names')
@click.argument('file_out', type=click.File('w'), default='-', required=False)
def search(names, file_out):
	"""Searchs Linkedin for company's basic info"""

	companyNames = clean_split(names,',')
	if not companyNames:
		print('Please use --name option to inform companies to be searched\n')
		exit(1)

	client_id, client_secret = ('77n2y1vmm70e79','MjfOQ2XA8sKavP9C')
	redirect_uri = 'http://127.0.0.1'

	authz_url = Linkedin.get_authorization_url(client_id,redirect_uri)

	# Try to get the authorization code.
	# At this point, a browser should open and the user should grant access to this app.
	# Then the app's server would receive a request in the 'request_uri' with the access code.
	# As this app do not support this functionality, we gonna fake an access code.
	authorization_code = 'AQTeo7N5xUNtUzngsUSuMDGZuKCnDiRKOjKRet-6ONHyj6saJy5_SJgacu05xuDFxdtHDmUMl3jegxmxCUoAjV3vlwBsFHLKy89t9jkhL44p7EZ-j6goVVBsHxHU7-MfZnttCn4-Ou7pqww-Vjk'

	# this client_id does not have permission to query Organization API, so this call will fail.
	access_token = Linkedin.get_token(client_id, client_secret, redirect_uri, authorization_code) or \
					"AQXDEI5dr4wGltaY7eg2m5aIVHqFtmkGDHQTDcFXh8z-Wv2PNsHnQYbt5S740uCQ3oLE4o6POvzXllbM7kCW7zsKTUxn9i1nvhp2eKLLt7zY4fmrxzI7zWud5b3R4ZcGpPlyqOIWrg0pzUdxJNgS9y-8iiqYnKvEwxU03BVUyFaLB56iKvsvLannXVrRyQTQl1Q_i6rpmgewSgMZV8Ac3swMpPfvPBfzUP5RWFOrYspO3VsdFA86SKayBCGd8dPeoXeV9GmvEaLF9s_QVZfOAQGJU4XhBYBAcsrvV1pKXXCe9k_EOeCF-l92rgXhhJI6qSFb75Tr8JZqZIS0ZmluELpIcnkRrQ"

	lnkdin = Linkedin(access_token)
	companyData = lnkdin.searchCompanies(companyNames)

	print(json.dumps(companyData, indent=4))
