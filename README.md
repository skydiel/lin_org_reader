# lin_org_reader
Linkedin Organization Reader

## Requirements

- Docker

## Installation instructions

- Clone this repo
- Enter the repo root folder
- Type the flollowing commands:

```docker build --tag lor:1.0 .```

```docker run --rm -e COMPANY_NAMES=[COMPANY_NAMES] lor:1.0```

*COMPANY_NAMES* is a string of company names separated by comma. If a company has space in its name,
it should be escaped or the whole list must be inside quotes.

```docker run --rm -e COMPANY_NAMES=Bianchi\ Inc,Harley\ Davidson lor:1.0```

or

```docker run --rm -e COMPANY_NAMES="Bianchi Inc,Harley Davidson" lor:1.0```

## Notes

The Linkedin App configured in the app does not have access to query the Organization API.
Because of that, some data are faked just to make the app work. Moreover, the workflow to
get a OAuth2 access token requires the set up of a callback and this app does not implement this.
All the steps to get a token is demonstrated in the app but actual token is faked.
