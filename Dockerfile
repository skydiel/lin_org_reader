FROM python:3.6

WORKDIR /app

ADD ./*.py ./

RUN pip install --trusted-host pypi.python.org --upgrade pip>=10 setuptools
RUN pip install -e .

ENV COMPANY_NAMES=""

CMD lnkdin search --names $COMPANY_NAMES