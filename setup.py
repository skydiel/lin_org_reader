from setuptools import setup

setup(
	name='lnkdin',
	version='1.0.0',
	install_requires=[
		'click',
		'requests',
		'faker'
	],
	entry_points='''
		[console_scripts]
			lnkdin=lnkdin:cli
	''',
)