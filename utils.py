def clean_split(text, sep=' '):
	"""Returns a list without empty elements""" 
	return list(filter(bool, text.strip(sep).split(sep)))

def filter_dict(d, attribs):
	return {k: d[k] for k in d if k in attribs}
